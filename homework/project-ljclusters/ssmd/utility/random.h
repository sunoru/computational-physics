#ifndef SSMD_UTILITY_RANDOM_H
#define SSMD_UTILITY_RANDOM_H

#include <openssl/evp.h>

namespace ssmd
{
class RandomGenerator
{
    public:
        RandomGenerator(unsigned long long seed);

        ~RandomGenerator();

        unsigned long long rand();

    private:
        const int buffer_length = 8000;
        int buffer_counter;
        unsigned long long counter;
        EVP_CIPHER_CTX *ctx;
        unsigned char *buffer;

        void update_buffer();
};

class RandomDistribution
{
    public:
        RandomDistribution(unsigned long long seed);

        ~RandomDistribution();

        double rand_uniform_real();
        // TODO long long rand_uniform_int(int a, int b);
    private:
        RandomGenerator *rnd;
};

}

#endif //SSMD_UTILITY_RANDOM_H
