#include <cassert>
#include <random>
#include <openssl/evp.h>
#include "random.h"


namespace ssmd
{
RandomGenerator::RandomGenerator(unsigned long long seed)
{
    counter = 0;
    buffer_counter = 0;
    unsigned char *key = new unsigned char[16]();
    for (int i = 0; i < 8; ++i)
    {
        key[i] = (unsigned char) (seed & 0xff);
        seed >>= 8;
    }
    ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);
    EVP_CIPHER_CTX_set_padding(ctx, false);
    buffer = new unsigned char[buffer_length]();
    update_buffer();
    delete[] key;
}

RandomGenerator::~RandomGenerator()
{
    EVP_CIPHER_CTX_free(ctx);
    delete[] buffer;
}

unsigned long long RandomGenerator::rand()
{
    if (buffer_counter == buffer_length >> 3)
    {
        update_buffer();
        buffer_counter = 0;
    }
    return ((unsigned long long *) buffer)[buffer_counter++];
}

void RandomGenerator::update_buffer()
{
    unsigned char *inbuf = new unsigned char[buffer_length]();
    int len;
    for (int i = 0; i < buffer_length >> 3; ++i)
    {
        ((unsigned long long *) inbuf)[i] = counter++;
    }

    EVP_EncryptUpdate(ctx, buffer, &len, inbuf, buffer_length);
    assert(len == buffer_length);

    delete[] inbuf;
}

RandomDistribution::RandomDistribution(unsigned long long seed)
{
    rnd = new RandomGenerator(seed);
}

RandomDistribution::~RandomDistribution()
{
    delete rnd;
}

double RandomDistribution::rand_uniform_real()
{
    return (double) rnd->rand() / 18446744073709551616.0;
}

}
