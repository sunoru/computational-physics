#ifndef LJCLUSTERS_GENETIC_H
#define LJCLUSTERS_GENETIC_H

#include <memory>
#include <cstdio>
#include "types.h"

std::shared_ptr<opt_result> genetic_algorithm(const size_t num_particles,
                                              const size_t num_individuals,
                                              const size_t num_matings,
                                              const double probability_mutation = 0.1,
                                              const size_t conjugrad_max_iterations = 20,
                                              const double conjugrad_step_size = 0.01,
                                              const double conjugrad_tolerance = 1.0e-4,
                                              const double delta_e = 0.02,
                                              const unsigned long long random_seed = 200701281,
                                              FILE *logfile = stdout);

#endif //LJCLUSTERS_GENETIC_H
