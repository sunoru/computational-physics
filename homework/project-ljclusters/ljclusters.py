#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

def main():
    import sys
    import subprocess
    from os import urandom
    assert len(sys.argv) == 4
    a, b, step = [int(x) for x in sys.argv[1:]]
    for n in xrange(a, b, step):
        random_seed = int(urandom(8).encode('hex'), 16)
        params = [
            "./ljclusters", # exe
            "log/t%d.xyz" % n, # coordination file (output)
            "%d" % n, # number of particles
            "8", # number of individuals
            "2000", # number of matings
            "0.1", # probability of mutation
            "200", # max number of iterations of conjugate gradient method
            "0.01", # step size in conjugrad
            "0.0001", # tolerance in conjugrad
            "0.02", # delta E
            "%d" % random_seed,
            "log/t%d.log" % n, # log file
        ]
        print ' '.join(params)
        subprocess.call(params)

if __name__ == "__main__":
    main()

