#!/usr/bin/python2.7
# -*- coding:utf-8 -*-

def main():
    import sys
    import os
    import pylab
    import numpy as np
    assert (len(sys.argv) == 2)
    with open(sys.argv[1]) as fi:
        data = np.array([(int(x[0]), float(x[1]), float(x[-1])) for x in [x.split()
            for x in fi.readlines()[9:-1]]]).transpose()
    pylab.plot(data[0], data[1], '-', label='minima')
    pylab.plot(data[0], data[2], '-', label='maxima')
    pylab.legend()
    pylab.xlabel('generations', size=14)
    pylab.ylabel('$E$', size=14)
    pylab.savefig(os.path.split(sys.argv[1])[-1][:-3] + 'eps', dpi=300, pad_inches=0.01)
    pylab.show()

if __name__ == "__main__":
    main()

