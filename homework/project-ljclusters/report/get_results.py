#!/usr/bin/python2.7
# -*- coding:utf-8 -*-

def main():
    import sys
    for i in xrange(len(sys.argv)-1):
        with open(sys.argv[1+i]) as fi, open('results.dat', 'a') as fo:
            x = fi.readlines()
            fo.write("%s\t%s\n" % (x[0][:-1], x[1][9:x[1].find(';')]))

if __name__ == "__main__":
    main()

