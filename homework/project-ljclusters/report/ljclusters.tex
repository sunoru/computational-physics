\documentclass{article}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage[font=small,skip=0pt]{caption}
\usepackage{csquotes}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{float}
\usepackage{listings}
\geometry{a4paper, margin = 1in}
\author{Dawei Si 3130000628}
\title{Energy minimization of Lennard-Jones clusters}
\date{January 10, 2016}

\begin{document}
\maketitle
\section{Introduction}
Minimizing the energy of atomic clusters is always a difficult task, for there are local minima of a
exponentially increasing number. Lennard-Jones potential is defined as:
\begin{equation}
    \nu(r)=\frac{1}{\mathbf{r}^{12}}-\frac{2}{\mathbf{r}^6}
\end{equation}
where $r$ denotes to the distance between two particles.
Many computational researches have been done to try to find a global minimum of the Lennard-Jones clusters.
For example, Wales et al.\cite{wales1997global} have used a technique named "Basin-Hopping" to do the global
optimization. They have maintained the \textit{Cambridge Cluster Database}
\footnote{\href{http://www-wales.ch.cam.ac.uk/CCD.html}http://www-wales.ch.cam.ac.uk/CCD.html}, where the
optimized data of a variety of clusters are stored. According to the \textit{Table of Lennard-Jones Cluster}
with $N=3-150$\footnote{\href{http://doye.chem.ox.ac.uk/jon/structures/LJ/tables.150.html}
http://doye.chem.ox.ac.uk/jon/structures/LJ/tables.150.html}most of the newest minima are provided by Northby
et al.\cite{northby1987structure} They have used an efficient lattice based search and optimization procedure,
which has been developed and used with various assumed pair potentials to find minimal energy structures on an
icosahedrally derived lattice. Taking these as initial structures, they have relaxed them freely to the adjacent
energy minimum. \\
What has appeeled to me is the work of Daven et al.\cite{daven1996structural} They have made use of an
interesting method that is named "genetic algorithm". This technique simulates the heredity and evolution of
a species to obtain the well evolved individual. It can be useful for general purpose optimizations in many
fields, and may also give good results for this problem.\\
In this work, I have also taken a genetic method to find the minimal energy structures of the Lennard-Jones
clusters. My method is roughly based on Daven's work, but I want to get it improved somehow. For the time
limited, the work is still incomplete but has already made some results to show.

\section{Methodology}
The purpose of this work is to obtain
\begin{equation}
    min\{E\}
\end{equation}
where
\begin{equation}
    E=\frac{1}{2}\sum_{0\le i,j<n, i\neq j}{E_{ij}}
    =\sum_{0\le i<n-1}\sum_{i<j<n}{E_{ij}}
    =\sum_{0\le i<n-1}\sum_{i<j<n}{(\mathbf{r}_{ij}^{-12}-2\mathbf{r}_{ij}^{-6})}
\end{equation}
\begin{equation}
    \mathbf{r}_{ij}=(x_i-x_j)\mathbf{i}+(y_i-y_j)\mathbf{j}+(z_i-z_j)\mathbf{k}
\end{equation}
All the locally optimizing procedure is done by the nonlinear conjugate gradient algorithm. It is a kind of
unconstrained optimization method based on the gradient of the function. The version I choose is the
Polak-Ribiere conjugate gradient method.
All the positions of the particles are regardded as an individual $\mathbf{a}$ and a state in the $3n$-dimension
hyperspace, represented by $\mathbf{a}=\{q_0, q_1, q_2, ...\}$. $x_0$, $y_0$ and $z_0$ are respectively transformed by
$q_0$, $q_{n+0}$ and $q_{2n+0}$, etc. Therefore, the gradient is defined by
\begin{equation}
    \nabla E(\mathbf{a})=\sum_{i=0}^{3n-1}{\frac{\partial E}{\partial q_i}\mathbf{e}_i}
    =\sum_{d=0}^3\sum_{i=0}^{n-1}{12\left(\sum_{0\le j<n,i\neq j} (\mathbf{r}_{ij}^{-8}-\mathbf{r}_{ij}^{-14})
    (q_{i+dn}-q_{j+dn})\right)\mathbf{e}_{i+dn}}
\end{equation}
where $\mathbf{e}_i$ is the unit vector along the direction of $q_i$ in the hyperspace.\\
Given a step direction $\mathbf{p}_k$, use the Brent line search method to find step length $\alpha_k$ such
that $\mathbf{x}_{k+1}=\mathbf{x}_k+\alpha_k\mathbf{p}_k$. Then compute
\begin{equation}
    \beta_{k+1}=\frac{\nabla E(\mathbf{q}_{k+1}).(\nabla E(\mathbf{q}_{k+1})-\nabla E(\mathbf{q}_k))}
    {\nabla E(\mathbf{q}_k).\nabla E(\mathbf{q}_k)}
\end{equation}
\begin{equation}
    \mathbf{p}_{k+1}=\beta_{k+1}\mathbf{p}_k-\nabla E(\mathbf{q}_{k+1})
\end{equation}
The maximal number of iterations in this work is set up to 200, though the original work by Daven et al.
only use a number less than 20. This should not affect the results much because the method can stop by itself.\\
The genetic algorithm is performed by following steps:\\
\begin{enumerate}
    \item Generate the initial generation. In this work the number of individuals is a small number less
        than 10, The original individuals are chose almost absolutely randomly. A series of real numbers
        are randomly generated to become the coordinates. And then use the conjugate gradient method to
        relax the structure to a minimum, which is probably a local one. When an individual is generated,
        it will be checked to not have any too close pair. To keep a good species diversity,
        a small $\delta E$ is used to be a limitation that each two individuals can not have energies with
        a difference within $\delta E$. These check methods are also used for the following steps.
    \item Matings. The evolution occurs when there is enough generations, which can be indicated by the number
        of matings. In this work it is set up to 2000. An individual $\mathbf{a}$ in a generation $\{\mathbf{a}\}$ is selected
        to be the "parent" randomly with a Boltzman factor
        \begin{equation}
            P(\mathbf{a})\propto \mathrm{exp}(-E(\mathbf{a})/T_m)
        \end{equation}
        where $E(a)$ is the energy per atom of $a$, and the "temperature" $T_m$ is chosen to be the range of
        energies in $\{a\}$. The mating action of two parents is to be cut by a randomly generated plane, the
        upper part of "father" and lower part of "mother" are then assembled together to yield the child. If
        the child does not has the same number of particles as the parents, the plane will be translated a
        little to make it. If the translation is impossible, reject the plane and generate another new plane.
        To avoid rejecting the planes all the time, a special mutation with a small probability will occur 
        to rotate one of the parents around its mass center, which ensures the energy is invariable.
    \item Selections. Optimize the new child to the closest local minimum with the conjugate gradient method.
        If there is an individual that has an energy higher than it, accept the child and discard the highest
        energy individual in the population. Following the rule about $\delta E$, The new child may be
        rejected for there has been already an individual with an energy close to it.
    \item Mutations. With a small probability about 0.1, an individual may mutate to be a new structure. The
        mutation method used in this work is to loop 5-50 times to randomly select an atom to be moved a
        random distance in a random direction. The mutation only occurs when an individual is generated in
        a mating and before the optimization.
    \item The result is the minimal energy individual in the whole procedure.
\end{enumerate}
The original work use one more mutation method, but it is temporarily not achieved in this work.\\
Another thing worthy to be mentioned is that all the random numbers are generated by my own pseudo random
number generator using AES cryptography method provided by OpenSSL. With a random seed provided by a true
random number generator, the data will be reliable enough.

\section{Results}
Because of the lack of time, I did only computations of some $N$, and I failed to get any new minimum of the
L-J clusters (within expected). The results are shown in Figure 1 and Table 1. To a very limited extend this
work gives results as good as the existing data, though the results have already been very close to them. I
think I can improve this method after my final exams.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{results.eps}
    \caption{Plots of results and the existing data}
\end{figure}
\begin{table}[H]
    \centering
    \caption{List of results and the existing data}
    \begin{tabular}{@{}lrrr|lrrr@{}}
        \toprule
        n & data in this work & existing data & difference & n & data in this work & existing data & difference \\ \midrule
        4  &   -6.000000 &   -6.000000 &  0.000000 & 45  & -213.784861 & -213.784862 &  +0.000001 \\
        5  &   -9.103852 &   -9.103852 &  0.000000 & 46  & -214.752865 & -220.680330 &  +5.927465 \\
        7  &  -16.505384 &  -16.505384 &  0.000000 & 48  & -226.139828 & -232.199529 &  +6.059701 \\
        9  &  -24.113353 &  -24.113360 & +0.000007 & 50  & -238.185156 & -244.549926 &  +6.364770 \\
        10 &  -28.422532 &  -28.422532 &  0.000000 & 52  & -253.223202 & -258.229991 &  +5.006789 \\
        15 &  -52.322627 &  -52.322627 &  0.000000 & 53  & -255.931574 & -265.203016 &  +9.271442 \\
        17 &  -61.317995 &  -61.317995 &  0.000000 & 54  & -266.856978 & -272.208631 &  +5.351653 \\
        19 &  -72.659782 &  -72.659782 &  0.000000 & 56  & -283.643105 & -283.643105 &   0.000000 \\
        20 &  -77.177042 &  -77.177043 & +0.000001 & 60  & -302.348839 & -305.875476 &  +3.526637 \\
        21 &  -81.684571 &  -81.684571 &  0.000000 & 62  & -307.156865 & -317.353901 & +10.197036 \\
        22 &  -86.809782 &  -86.809782 &  0.000000 & 66  & -332.504953 & -341.110599 &  +8.605646 \\
        23 &  -92.844472 &  -92.844472 &  0.000000 & 70  & -366.892251 & -366.892251 &   0.000000 \\
        24 &  -97.348815 &  -97.348815 &  0.000000 & 72  & -368.126900 & -378.637253 & +10.510353 \\
        25 & -102.372663 & -102.372663 &  0.000000 & 81  & -430.362386 & -434.343643 &  +3.981257 \\
        26 & -108.315616 & -108.315616 &  0.000000 & 87  & -471.383201 & -472.098165 &  +0.714964 \\
        30 & -128.286571 & -128.286571 &  0.000000 & 88  & -475.598704 & -479.032630 &  +3.433926 \\
        32 & -138.488490 & -139.635524 & +1.147034 & 98  & -535.022074 & -543.665361 &  +8.643287 \\
        36 & -161.825363 & -161.825363 &  0.000000 & 110 & -599.731736 & -621.788224 & +22.056488 \\
        40 & -182.283055 & -185.249839 & +2.966784 & 118 & -650.509393 & -674.769635 & +24.260242 \\
        43 & -200.433745 & -202.364664 & +1.930919 & & & & \\ \bottomrule
    \end{tabular}
\end{table}
Take $N=56$ for example, the parameters in the calculation I used are:
\begin{displayquote}
    number of particles: 56 \\
    number of individuals: 8 \\
    number of matings: 2000 \\
    probability of mutation: 0.1000000000 \\
    conjugrad's max iterations: 200 \\
    conjugrad's step size: 0.0100000000 \\
    conjugrad's tolerance: 0.0001000000 \\
    delta E: 0.0200000000 \\
    random seed: 952689271937337109
\end{displayquote}
The minima and maxima of the generations are plotted as Figure 2. It indicates that 2000 is a just-good
number of generations for $N=56$ in my current method. Maybe I should use a much larger number for the
large $N$. Of course, imporving my method is more neccessary.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.8]{t56.eps}
    \caption{Plots of the minima and maxima of $N=56$}
\end{figure}

\section{Conclusion}
In this work, the genetic algorithm of my version have done a not bad work, Although no new lowest energy
data is achieved. The results hit many existing data, and they do not have as large difference as $4\%$.
Improvement is needed to produce more precise results. Maybe a more specific mutation action or constraints
should be used, such as to make the cluster structures as symmetric as possible.\\
Certainly, the most important thing in this work is that I have learned much about the computational physics
with different methods. There were a few problems when I wrote the codes, for example it is sometimes
difficult to perform idea perfectly. All the source codes can be found on my bitbucket repository.
\footnote{\href{https://bitbucket.org/sunoru/computational-physics/src/}https://bitbucket.org/sunoru/computational-physics/src/} \\
\\
Thank you for the lessons!

\renewcommand\refname{Reference}
\bibliographystyle{unsrt}
\bibliography{ljclusters}
\end{document}
