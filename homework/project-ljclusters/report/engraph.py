#!/usr/bin/python2.7
# -*- coding:utf-8 -*-

def main():
    import pylab
    import numpy as np
    def open_data(filename):
        with open(filename) as fi:
            return [
                (lambda x: (int(x[0]), float(x[1])))
                (line.split()) for line in fi.readlines()
            ]
    s = open_data('standard.dat')
    standard = np.array(s).transpose()
    p = open_data('results.dat')
    p.sort(key=lambda x: x[0])
    results = np.array(p).transpose()
    pylab.plot(standard[0], standard[1], '.-', label='existing data')
    pylab.plot(results[0], results[1], 'o', label='data in this work')
    pylab.legend()
    pylab.xlabel('$n$', size=14)
    pylab.ylabel('$E$', size=14)
    pylab.savefig('results.eps', dpi=300, pad_inches=0.01)
    pylab.show()
    for e1 in p:
        a = 0
        for e2 in s:
            if e1[0] == e2[0]:
                a = e2[1]
                break
        print "%4d%12.6f%12.6f%12.6f" % (e1[0], e1[1], a, e1[1] - a)

if __name__ == "__main__":
    main()

