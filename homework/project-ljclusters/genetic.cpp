#define GENETIC_DEBUG

#include <cstdio>
#include <memory>
#include <cmath>
#include "genetic.h"
#include "ssmd/utility/random.h"
#include "conjugate_gradient.h"
#include "genetic_helper.h"

std::shared_ptr<opt_result> genetic_algorithm(const size_t num_particles,
                                              const size_t num_individuals,
                                              const size_t num_matings,
                                              const double probability_mutation,
                                              const size_t conjugrad_max_iterations,
                                              const double conjugrad_step_size,
                                              const double conjugrad_tolerance,
                                              const double delta_e,
                                              const unsigned long long random_seed,
                                              FILE *logfile)
{
    fprintf(logfile, "number of particles: %ld\n"
            "number of individuals: %ld\n"
            "number of matings: %ld\n"
            "probability of mutation: %.10f\n"
            "conjugrad's max iterations: %ld\n"
            "conjugrad's step size: %.10f\n"
            "conjugrad's tolerance: %.10f\n"
            "delta E: %.10f\n"
            "random seed: %lld\n",
            num_particles, num_individuals,
            num_matings, probability_mutation,
            conjugrad_max_iterations, conjugrad_step_size,
            conjugrad_tolerance, delta_e, random_seed);
    ssmd::RandomDistribution random(random_seed);
    generation gen;
    make_generation(gen, num_particles,
                    num_individuals,
                    conjugrad_max_iterations,
                    conjugrad_step_size,
                    conjugrad_tolerance,
                    delta_e,
                    random);

    svector probability_mating(num_individuals);
    size_t ngen = 0;
    std::shared_ptr<individual> hall_of_fame(new individual);
    hall_of_fame->x = std::shared_ptr<svector>(new svector(num_particles * 3));
    copy_individual(gen.front().get(), hall_of_fame.get());
    do
    {
#ifdef GENETIC_DEBUG
        fprintf(logfile, "%5ld", ngen);
        for (const auto &ind:gen)
        {
            fprintf(logfile, "%15.5f", ind->energy);
        }
        fprintf(logfile, "\n");
        //TODO : log only the first and last ind.
#endif
        generate_probability_mating(probability_mating, gen);
        auto parent_a = select_parent(probability_mating, gen, random);
        auto parent_b = select_parent(probability_mating, gen, random);
        while (parent_a == parent_b)
            parent_b = select_parent(probability_mating, gen, random);

        auto child = mate(*parent_a, *parent_b, 0, conjugrad_max_iterations,
                          conjugrad_step_size, conjugrad_tolerance, random);
        if (random.rand_uniform_real() < probability_mutation)
        {
            mutate(*child, random);
        };
        conjugrad(*child, conjugrad_max_iterations,
                  conjugrad_step_size, conjugrad_tolerance);

        //keep_generation(gen);
        if (child->energy < gen.back()->energy && check_insert(gen, child, delta_e))
            gen.pop_back();
        if (gen.front()->energy < hall_of_fame->energy)
            copy_individual(gen.front().get(), hall_of_fame.get());
        ngen++;
    } while (ngen <= num_matings);

    return hall_of_fame;
}
