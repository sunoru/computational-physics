#ifndef LJCLUSTERS_CONJUGATE_GRADIENT_H
#define LJCLUSTERS_CONJUGATE_GRADIENT_H


#include <memory>
#include "types.h"

void conjugrad(opt_result &ind,
               const size_t &max_iterations = 1000,
               const double step_size = 0.01,
               const double tolerance = 1.0e-4);


#endif //LJCLUSTERS_CONJUGATE_GRADIENT_H
