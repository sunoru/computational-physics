//#define GENETIC_HELPER_DEBUG
#ifdef GENETIC_HELPER_DEBUG

#include <cstdio>

#endif

#include <cmath>
#include <utility>
#include <algorithm>
#include "genetic_helper.h"
#include "conjugate_gradient.h"
#include "lj_potential.h"

#define loop(n) for (size_t i = 0; i < (n); ++i)

static inline std::shared_ptr<individual> generate_init_individual(const size_t num_particles,
                                                            ssmd::RandomDistribution &random,
                                                            const size_t conjugrad_max_iterations,
                                                            const double conjugrad_step_size,
                                                            const double conjugrad_tolerance)
{
    std::shared_ptr<individual> ind(new individual);
    ind->x = std::shared_ptr<svector>(new svector(num_particles * 3));
    double r = pow(num_particles, 0.33);
    for (size_t i = 0; i < num_particles * 3; ++i)
    {
        (*ind->x)[i] = (random.rand_uniform_real() - 0.5) * r;
    }
    conjugrad(*ind, conjugrad_max_iterations, conjugrad_step_size,
              conjugrad_tolerance);
    return ind;
}

bool check_insert(generation &gen,
                  const std::shared_ptr<individual> ind_p, const double delta_e)
{
    size_t l = 0, r = gen.size();
    if (r != 0)
    {
        double ve = ind_p->energy;
        loop(r)
            if (fabs(gen[i]->energy - ve) < delta_e)
                return false;
        while (l < r - 1)
        {
            size_t m = (l + r) >> 1;
            double de = gen[m]->energy - ve;
            /*if (fabs(de) < delta_e)
                return false;*/
            if (de < 0)
                l = m;
            else
                r = m;
        }
        /*
        if (gen[l]->energy > ve)
        {
            if (gen[l]->energy - ve < delta_e)
                return false;
            r--;
        }*/
    }
    gen.insert(gen.begin() + r, ind_p);

    return true;
}

static inline double desqr(double x, double y)
{ return (x - y) * (x - y); }

static inline bool check(const svector &x)
{
    size_t n = x.size() / 3;
    for (size_t i = 0; i < n - 1; ++i)
        for (size_t j = i + 1; j < n; ++j)
            if (desqr(x[i], x[j]) +
                desqr(x[i + n], x[j + n]) +
                desqr(x[i + (n << 1)], x[j + (n << 1)]) < 0.5)
                return false;
    return true;
}

void make_generation(generation &gen,
                     const size_t num_particles,
                     const size_t num_individuals,
                     const size_t conjugrad_max_iterations,
                     const double conjugrad_step_size,
                     const double conjugrad_tolerance,
                     const double delta_e,
                     ssmd::RandomDistribution &random)
{
    for (size_t i = 0; i < num_individuals; ++i)
    {
        std::shared_ptr<individual> ind_p;
        do
        {
            ind_p = generate_init_individual(num_particles, random,
                                             conjugrad_max_iterations,
                                             conjugrad_step_size,
                                             conjugrad_tolerance);
        } while (!check(*ind_p->x) || !check_insert(gen, ind_p, delta_e));
    }
}

void generate_probability_mating(svector &probability_mating, const generation &gen)
{
    // the method of calculating temperature should be checked.
    double scale = (gen.back()->energy - gen.front()->energy) * gen.front()->x->size();

    probability_mating[0] = exp(gen[0]->energy / scale);
    for (size_t i = 1; i < gen.size(); ++i)
        probability_mating[i] = exp(gen[i]->energy / scale) + probability_mating[i - 1];
    loop(gen.size())
        probability_mating[i] /= probability_mating.back();
#ifdef GENETIC_HELPER_DEBUG
    loop(gen.size())
        printf("%15.8lf", probability_mating[i]);
    printf("\n");
#endif

}

std::shared_ptr<individual> select_parent(const svector &probability_mating, const generation &gen,
                                          ssmd::RandomDistribution &random)
{
    auto p = random.rand_uniform_real();
    size_t i = 0;
    while (probability_mating[i] < p)
        i++;
    return gen[i];
}

static inline svector random_vector(size_t size, ssmd::RandomDistribution &random)
{
    svector a;
    loop(size)
    {
        a.push_back(random.rand_uniform_real() - 0.5);
    }
    return a;
}

static inline void make_center(svector &coords)
{
    size_t n = coords.size() / 3;
    svector center(3);
    loop(n)
    {
        center[0] += coords[i];
        center[1] += coords[i + n];
        center[2] += coords[i + (n << 1)];
    }
    center[0] /= n;
    center[1] /= n;
    center[2] /= n;
    loop(n)
    {
        coords[i] -= center[0];
        coords[i + n] -= center[1];
        coords[i + (n << 1)] -= center[2];
    }
}

static inline svector normal_vector(const svector &plane)
{
    svector a(3);
    a[0] = plane[2] * plane[5] - plane[4] * plane[3];
    a[1] = plane[4] * plane[2] - plane[0] * plane[5];
    a[2] = plane[0] * plane[3] - plane[2] * plane[1];
    return a;
}

static inline double sqr(const double a)
{ return a * a; }

static inline double module(const svector &a)
{
    return sqrt(sqr(a[0]) + sqr(a[1]) + sqr(a[2]));
}

static inline double dot(const svector &a, const double x, const double y, const double z)
{
    return a[0] * x + a[1] * y + a[2] * z;
}

static inline std::vector<std::pair<size_t, double>> make_distances(const svector &vect, const size_t n,
                                                             const svector &nv, const double nv_module)
{
    std::vector<std::pair<size_t, double>> result;
    loop(n)
        result.push_back(std::make_pair(i, dot(nv, vect[i], vect[i + n], vect[i + (n << 1)])/nv_module));
    return result;
};

static inline bool dist_cmp(const std::pair<size_t, double> &a, const std::pair<size_t, double> &b)
{
    return a.second < b.second;
}

static inline size_t find_sep(const std::vector<std::pair<size_t, double>> a)
{
    size_t l = 0, r = a.size() - 1;
    while (l < r - 1)
    {
        size_t m = (l + r) >> 1;
        if (a[m].second < 0)
            l = m;
        else
            r = m;
    }
    return l;
}

void mutate(individual &ind, ssmd::RandomDistribution &random)
{
    size_t n = ind.x->size() / 3;
    size_t tn = (size_t) (random.rand_uniform_real() * 50 + 5);
    double d = ind.energy / n / (n + 1) * 2;
    double mean_r = pow((-1 + sqrt(1 + d)) / d, 1.0 / 6);
    mean_r = 1;
    loop(tn)
    {
        size_t j = (size_t) (random.rand_uniform_real() * n * 3);
        (*ind.x)[j] += (random.rand_uniform_real() - 0.5) * 2 * mean_r;
    }
    if (!check(*ind.x))
        mutate(ind, random);
    else
        ind.energy = lj_potential(*ind.x);
}

static inline double xp(const double x, const double y, const double z,
                 const double a, const double b, const double c)
{
    return cos(b) * cos(c) * x + sin(c) * cos(b) * y - sin(b) * z;
}

static inline double yp(const double x, const double y, const double z,
                 const double a, const double b, const double c)
{
    return (sin(a) * sin(b) * cos(c) - sin(c) * cos(a)) * x +
           (sin(a) * sin(b) * sin(c) + cos(a) * cos(c)) * y +
           sin(a) * cos(b) * z;
}

static inline double zp(const double x, const double y, const double z,
                 const double a, const double b, const double c)
{
    return (sin(a) * sin(c) + sin(b) * cos(a) * cos(c)) * x +
           (-sin(a) * cos(c) + sin(b) * sin(c) * cos(a)) * y +
           cos(a) * cos(b) * z;
}

void mutate2(individual &ind, ssmd::RandomDistribution &random)
{
    size_t n = ind.x->size() / 3;
    double a = (double) (random.rand_uniform_real() * M_PIl * 2);
    double b = (double) (random.rand_uniform_real() * M_PIl * 2);
    double c = (double) (random.rand_uniform_real() * M_PIl * 2);
    loop(n)
    {
        double x = (*ind.x)[i];
        double y = (*ind.x)[i + n];
        double z = (*ind.x)[i + (n << 1)];
        (*ind.x)[i] = xp(x, y, z, a, b, c);
        (*ind.x)[i + n] = yp(x, y, z, a, b, c);
        (*ind.x)[i + (n << 1)] = zp(x, y, z, a, b, c);
    }
    ind.energy = lj_potential(*ind.x);
}


std::shared_ptr<individual>
mate(individual &parent_a, individual &parent_b,
     const double probability_mutation,
     const size_t conjugrad_max_iterations,
     const double conjugrad_step_size,
     const double conjugrad_tolerance,
     ssmd::RandomDistribution &random)
{
    std::shared_ptr<individual> child(new individual());
    size_t n = parent_a.x->size() / 3;
    make_center(*parent_a.x);
    make_center(*parent_b.x);
    size_t sep;
    size_t iter = 0;
    while (true)
    {
        iter++;
        if (iter > 5 || random.rand_uniform_real() < probability_mutation)
        {
            iter = 0;
            mutate2(parent_b, random);
            //conjugrad(parent_b, conjugrad_max_iterations, conjugrad_step_size, conjugrad_tolerance);
        };
        svector plane = random_vector(6, random);
        svector nv = normal_vector(plane);
        double nv_module = module(nv);
        auto dist_a = make_distances(*parent_a.x, n, nv, nv_module);
        auto dist_b = make_distances(*parent_b.x, n, nv, nv_module);
        std::sort(dist_a.begin(), dist_a.end(), dist_cmp);
        std::sort(dist_b.begin(), dist_b.end(), dist_cmp);
        size_t sep_a = find_sep(dist_a);
        size_t sep_b = find_sep(dist_b);
        if (sep_b < sep_a)
        {
            auto t = sep_a;
            sep_a = sep_b;
            sep_b = t;
        }
        bool flag = false;
        for (sep = sep_a; sep <= sep_b; ++sep)
            if (dist_a[sep].second < dist_b[sep + 1].second && dist_a[sep + 1].second > dist_b[sep].second)
            {
                flag = true;
                break;
            }
        if (!flag)
            for (sep = 0; sep < n; ++sep)
                if (sep == sep_a)
                {
                    sep = sep_b + 1;
                    continue;
                }
                else if (dist_a[sep].second < dist_b[sep + 1].second && dist_a[sep + 1].second > dist_b[sep].second)
                {
                    flag = true;
                    break;
                }
        if (flag)
        {
            child->x = std::shared_ptr<svector>(new svector(parent_a.x->size()));
            loop(sep + 1)
            {
                (*child->x)[i] = (*parent_a.x)[dist_a[i].first];
                (*child->x)[i + n] = (*parent_a.x)[dist_a[i].first + n];
                (*child->x)[i + (n << 1)] = (*parent_a.x)[dist_a[i].first + (n << 1)];
            }
            for (size_t i = sep + 1; i < n; ++i)
            {
                (*child->x)[i] = (*parent_b.x)[dist_b[i].first];
                (*child->x)[i + n] = (*parent_b.x)[dist_b[i].first + n];
                (*child->x)[i + (n << 1)] = (*parent_b.x)[dist_b[i].first + (n << 1)];
            }
            if (check(*child->x))
            {
                child->energy = lj_potential(*child->x);
                break;
            }
        }
    }

    return child;
}

static inline bool ind_cmp(const std::shared_ptr<individual> inda,
                    const std::shared_ptr<individual> indb)
{
    return inda->energy < indb->energy;
}

void keep_generation(generation &gen)
{
    std::sort(gen.begin(), gen.end(), ind_cmp);
}

void copy_individual(const individual *src, individual *dest)
{
    std::copy(src->x->begin(), src->x->end(), dest->x->begin());
    dest->energy = src->energy;
    dest->num_of_iterations = src->num_of_iterations;
}
