//#define CONJUGATE_DEBUG
#ifdef CONJUGATE_DEBUG

#include <iostream>
#include <iomanip>

#endif

#include <memory>
#include <vector>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include "conjugate_gradient.h"
#include "lj_potential.h"

#define loop(n) for (size_t i = 0; i < (n); ++i)

void conjugrad(individual &ind,
              const size_t &max_iterations,
              const double step_size,
              const double tolerance)
{
    size_t n = ind.x->size();
    size_t *params = new size_t[0]();
    params[0] = n;
    gsl_multimin_function_fdf my_func;
    my_func.f = &gsl_lj_potential;
    my_func.df = &gsl_lj_potential_gradient;
    my_func.fdf = &gsl_lj_potential_and_gradient;
    my_func.n = n;
    my_func.params = params;
    auto gsl_x0 = to_gsl_vector(*ind.x);
    const gsl_multimin_fdfminimizer_type *gsl_minimizer_type =
    gsl_multimin_fdfminimizer_conjugate_pr;
    gsl_multimin_fdfminimizer *gsl_minimizer =
    gsl_multimin_fdfminimizer_alloc(gsl_minimizer_type, n);

    gsl_multimin_fdfminimizer_set(gsl_minimizer, &my_func,
                                  gsl_x0, step_size, tolerance);
    size_t iteration = 0;
    int status = 0;
    do
    {
        iteration++;
        status = gsl_multimin_fdfminimizer_iterate(gsl_minimizer);
        if (status)
        {
            if (status == 27)
                break;
            else
                throw new std::runtime_error("iterate error");
        }
        status = gsl_multimin_test_gradient(gsl_minimizer->gradient,
                                            tolerance);
#ifdef CONJUGATE_DEBUG
        if (status == GSL_SUCCESS)
            std::cout << "Minimum found at: " << std::endl;
        std::cout << std::setw(3) << iteration << " " << std::fixed
        << std::setprecision(10);
//        loop(n)
//            std::cout << std::setw(10) << gsl_vector_get(gsl_minimizer->x, i) << " ";
        std::cout << std::setw(12) << gsl_minimizer->f << std::endl;
#endif
    } while (status == GSL_CONTINUE && iteration < max_iterations);

    ind.x = to_vector(gsl_minimizer->x, n);
    ind.energy = gsl_minimizer->f;
    ind.num_of_iterations = iteration;
    gsl_multimin_fdfminimizer_free(gsl_minimizer);
    gsl_vector_free(gsl_x0);
    delete[] params;
}
