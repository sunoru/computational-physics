#ifndef LJCLUSTERS_TYPES_H
#define LJCLUSTERS_TYPES_H

#include <memory>
#include <vector>
#include <list>

typedef std::vector<double> svector;
typedef std::vector<svector> smatrix;

struct opt_result
{
    std::shared_ptr<svector> x;
    double energy;
    unsigned long num_of_iterations;
};

typedef opt_result individual;

//typedef std::list<std::shared_ptr<individual>> generation;
typedef std::vector<std::shared_ptr<individual>> generation;
// even if we use insertion sort, it doesn't make trouble for the size <= 10.

#endif //LJCLUSTERS_TYPES_H
