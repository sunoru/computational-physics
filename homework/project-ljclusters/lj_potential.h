#ifndef LJCLUSTERS_LJ_POTENTIAL_H
#define LJCLUSTERS_LJ_POTENTIAL_H

#include <vector>
#include <memory>
#include <gsl/gsl_vector_double.h>
#include "types.h"

std::shared_ptr<smatrix> calc_distance(const svector &x);

double lj_potential(const svector &x);

std::shared_ptr<svector> lj_potential_gradient(const svector &x);

double gsl_lj_potential(const gsl_vector *x, void *params);

void gsl_lj_potential_gradient(const gsl_vector *x, void *params, gsl_vector *df);

void gsl_lj_potential_and_gradient(const gsl_vector *x, void *params, double *f, gsl_vector *df);

std::shared_ptr<svector> to_vector(const gsl_vector *x, size_t n);

gsl_vector *to_gsl_vector(const svector &x);

#endif //LJCLUSTERS_LJ_POTENTIAL_H
