#ifndef LJCLUSTERS_GENETIC_HELPER_H
#define LJCLUSTERS_GENETIC_HELPER_H

#include "types.h"
#include "ssmd/utility/random.h"

void make_generation(generation &gen,
                     const size_t num_particles,
                     const size_t num_individuals,
                     const size_t conjugrad_max_iterations,
                     const double conjugrad_step_size,
                     const double conjugrad_tolerance,
                     const double delta_e,
                     ssmd::RandomDistribution &random);

void generate_probability_mating(svector &probability_mating,
                                 const generation &gen);

std::shared_ptr<individual> select_parent(const svector &probability_mating,
                                          const generation &gen,
                                          ssmd::RandomDistribution &random);

std::shared_ptr<individual>
mate(individual &parent_a, individual &parent_b,
     const double probability_mutation,
     const size_t conjugrad_max_iterations,
     const double conjugrad_step_size,
     const double conjugrad_tolerance,
     ssmd::RandomDistribution &random);

void mutate(individual &ind, ssmd::RandomDistribution &random);

bool check_insert(generation &gen,
                  const std::shared_ptr<individual> ind_p,
                  const double delta_e);

void keep_generation(generation &gen);

void copy_individual(const individual *src, individual *dest);

#endif //LJCLUSTERS_GENETIC_HELPER_H
