#include <assert.h>
#include "types.h"
#include "iohelper.h"

#define loop(n) for (size_t i = 0; i < (n); ++i)

void print_vector(const svector &vect, FILE *outfile)
{
    for (auto &element : vect)
    {
        fprintf(outfile, "%20.10lf", element);
    }
    printf("\n");
}

void print_result(const opt_result &result, FILE *outfile)
{
    size_t n = result.x->size();
    assert(n % 3 == 0);
    n /= 3;
    fprintf(outfile, "%ld\nenergy = %.10lf; iterations = %ld\n",
            n, result.energy, result.num_of_iterations);
    loop(n)
        fprintf(outfile, "He %20.10lf%20.10lf%20.10lf\n",
                (*result.x)[i], (*result.x)[i + n], (*result.x)[i + (n << 1)]);
}
