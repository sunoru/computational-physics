#include <cstdio>
#include <cassert>
#include "conjugate_gradient.h"
#include "iohelper.h"
#include "genetic.h"

// argv are the following variables.
int main(int argc, char *argv[])
{
    size_t num_particles;
    size_t num_individuals;
    size_t num_matings;
    double probability_mutation;
    size_t conjugrad_max_iterations;
    double conjugrad_step_size;
    double conjugrad_tolerance;
    double delta_e;
    unsigned long long random_seed;
    FILE *logfile = stdout;

    if (argc == 12)
    {
        logfile = fopen(argv[11], "w");
    }
    else
    {
        assert(argc == 11);
    }
    sscanf(argv[2], "%ld", &num_particles);
    sscanf(argv[3], "%ld", &num_individuals);
    sscanf(argv[4], "%ld", &num_matings);
    sscanf(argv[5], "%lf", &probability_mutation);
    sscanf(argv[6], "%ld", &conjugrad_max_iterations);
    sscanf(argv[7], "%lf", &conjugrad_step_size);
    sscanf(argv[8], "%lf", &conjugrad_tolerance);
    sscanf(argv[9], "%lf", &delta_e);
    sscanf(argv[10], "%lld", &random_seed);

    auto result = genetic_algorithm(num_particles,
                                    num_individuals,
                                    num_matings,
                                    probability_mutation,
                                    conjugrad_max_iterations,
                                    conjugrad_step_size,
                                    conjugrad_tolerance,
                                    delta_e,
                                    random_seed,
                                    logfile);

    fprintf(logfile, "Hall of Fame: %.10f\n", result->energy);
    FILE *outfile = fopen(argv[1], "w");
    print_result(*result, outfile);
    fclose(outfile);

    fclose(logfile);
    return 0;
}
