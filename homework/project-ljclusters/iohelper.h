#ifndef LJCLUSTERS_IOHELPER_H
#define LJCLUSTERS_IOHELPER_H

#include <cstdio>
#include "types.h"

void print_vector(const svector &vect, FILE *outfile=stdout);
void print_result(const opt_result &result, FILE *outfile=stdout);

#endif //LJCLUSTERS_IOHELPER_H
