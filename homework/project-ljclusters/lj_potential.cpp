#include <memory>
#include <cassert>
#include <cmath>
#include <gsl/gsl_vector_double.h>
#include "lj_potential.h"

#define loop(n) for (size_t i = 0; i < (n); ++i)

static inline double desqr(double x, double y)
{ return (x - y) * (x - y); }


static inline double lj_potential_ij(double r2)
{
    return pow(r2, -6) - 2.0 * pow(r2, -3);
}

static inline double lj_potential_gradient_ij(double r2)
{
    return pow(r2, -4) - pow(r2, -7);
}

std::shared_ptr<svector> to_vector(const gsl_vector *x, size_t n)
{
    std::shared_ptr<svector> xi(new svector(n));
    loop(n)
    {
        (*xi)[i] = gsl_vector_get(x, i);
    }
    return xi;
};

gsl_vector *to_gsl_vector(const svector &x)
{
    size_t n = x.size();
    gsl_vector *result = gsl_vector_alloc(n);
    loop(n)
        gsl_vector_set(result, i, x[i]);
    return result;
}

std::shared_ptr<smatrix> calc_distance(const svector &x)
{
    size_t n = x.size() / 3;
    std::shared_ptr<smatrix> u(new smatrix(n, svector(n)));
    for (size_t i = 0; i < n - 1; ++i)
        for (size_t j = i + 1; j < n; ++j)
            (*u)[i][j] = desqr(x[i], x[j]) +
                         desqr(x[i + n], x[j + n]) +
                         desqr(x[i + (n << 1)], x[j + (n << 1)]);
    return u;
}

static inline double lj_potential_internal(const svector &x, const smatrix &u)
{
    size_t n = x.size() / 3;
    double potential = 0;
    for (size_t i = 0; i < n - 1; ++i)
        for (size_t j = i + 1; j < n; ++j)
        {
            potential += lj_potential_ij(u[i][j]);
        }
    return potential;
}

double lj_potential(const svector &x)
{
    auto u = calc_distance(x);
    return lj_potential_internal(x, *u);
}

static inline std::shared_ptr<svector> lj_potential_gradient_internal(const svector &x, smatrix &u)
{
    size_t n = x.size() / 3;
    std::shared_ptr<svector> result(new svector(x.size()));
    for (size_t i = 0; i < n - 1; ++i)
        for (size_t j = i + 1; j < n; ++j)
        {
            u[i][j] = lj_potential_gradient_ij(u[i][j]);
        }
    loop(n)
        for (int d = 0; d < 3; ++d)
        {
            double p = 0;
            for (size_t j = 0; j < i; ++j)
                p += u[j][i] * (x[i + d * n] - x[j + d * n]);
            for (size_t j = i + 1; j < n; ++j)
                p += u[i][j] * (x[i + d * n] - x[j + d * n]);
            (*result)[i + d * n] = 12 * p;
        }
    return result;
}

std::shared_ptr<svector> lj_potential_gradient(const svector &x)
{
    auto u = calc_distance(x);
    return lj_potential_gradient_internal(x, *u);
}

double gsl_lj_potential(const gsl_vector *x, void *params)
{
    size_t n = ((size_t *) params)[0];
    assert(n % 3 == 0);
    return lj_potential(*to_vector(x, n));
}

void gsl_lj_potential_gradient(const gsl_vector *x, void *params, gsl_vector *df)
{
    size_t n = ((size_t *) params)[0];
    assert(n % 3 == 0);
    auto result = lj_potential_gradient(*to_vector(x, n));
    loop(n)
    {
        gsl_vector_set(df, i, (*result)[i]);
    }
}

void gsl_lj_potential_and_gradient(const gsl_vector *x, void *params, double *f, gsl_vector *df)
{
    size_t n = ((size_t *) params)[0];
    auto xx = to_vector(x, n);
    auto u = calc_distance(*xx);
    *f = lj_potential_internal(*xx, *u);
    auto result = *lj_potential_gradient_internal(*xx, *u);
    loop(n)
    {
        gsl_vector_set(df, i, result[i]);
    }
}
