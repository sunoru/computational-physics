c******************************************************************************
c This file includes a subroutine to provide a method that uses the Verlet
c approach to calculate an undamped, driven pendulum assuming q=0 with certain
c g, l,c FD and omegaD.
c by Dawei Si.
c******************************************************************************

c od: omega D. th0: theta 0. o0: omega 0. delt: delta t.
c endt: total time. fn: log file name.
      subroutine calc(g, l, fd, od, th0, o0, delt, endt, fn)
      implicit none 
      double precision g, l, fd, od, th0, o0, delt, endt
      character*(*) fn
c i1, i2, i3: i-2, i-1, i.
      integer i1, i2, i3, p1
c th(3): the newest three values of theta. o: omega. t: time
c ftt: second derivative of f.
      double precision th(3), o, t, ftt
      i1 = 1
      i2 = 2
      i3 = 3
      t = 0
      th(i1) = th0
      th(i2) = th0
      th(i3) = 0
      o = o0

      open(unit=7, file=fn, status='unknown')
      write(7, 101) t, th0, o0
      t = t + delt
      write(7, 101) t, th0, o0
      t = t + delt

      do while (t <= endt)
          ftt = - g / l * sin(th(i2)) + fd * sin(od * (t - delt))
          th(i3) = 2d0 * th(i2) - th(i1) + ftt * delt * delt
          o = (th(i3) - th(i1)) / 2d0 / delt
          write(7, 101) t, th(i3), o
          t = t + delt
          p1 = i1
          i1 = i2
          i2 = i3
          i3 = p1
      enddo
      close(unit=7)
101    format(' ', E20.9, E20.9, E20.9)
      endsubroutine
