#!/usr/bin/python
# coding=utf-8

# call the calc subroutine written in the fortran file.
def main():
    from driven_pendulum import calc
    fds = [0.1, 0.5, 0.99]
    g, l, od = 9.8, 9.8, 0.666
    th0 = 0.2
    o0 = 0
    delt = float(raw_input('delta t: '))
    endt = float(raw_input('total t: '))
    cl = lambda x: str(x).replace('.', '_')
    for fd in fds:
        fn = "dp-{delt}-{endt}-{fd}.log".format(
            delt=cl(delt), endt=cl(endt), fd=cl(fd)
        )
        print fn
        calc(g, l, fd, od, th0, o0, delt, endt, fn)


# analysis the data.
def main2():
    import numpy as np
    import pylab
    def normalize(x):
        while x > np.pi:
            x -= 2 * np.pi
        while x < -np.pi:
            x += 2 * np.pi
        return x
    fds = [0.1, 0.5, 0.99]
    delts = [0.001, 0.002]
    endt = 200.0
    cl = lambda x: str(x).replace('.', '_')
    for fd in fds:
        # To plot the theta-time figure.
        pylab.figure()
        pylab.xlabel("time / $s$", size=14)
        pylab.ylabel("$\\theta$ / rad", size=14)
        # set the x and y axis label
        for delt in delts:
            fn = "dp-{delt}-{endt}-{fd}.log".format(
                delt=cl(delt), endt=cl(endt), fd=cl(fd)
            ) # the log file name
            print fn
            with open(fn) as fi:
                a = np.array([map(float, each.split())
                    for each in fi.read().split('\n')[:-1]]).transpose()
            a[1] = np.array(map(normalize, a[1]))
            if fd >= 0.5 and delt == 0.002:
                # remove the latter half of the curve with delt = 0.002
                l = len(a[0])
                pylab.plot(a[0][:l/2], a[1][:l/2], '-', label='$\Delta t=%s$' % delt)
            else:
                pylab.plot(a[0], a[1], '-', label='$\Delta t=%s$' % delt)
        yl1, yl2 = pylab.ylim()
        if yl1 <= -np.pi and yl2 >= np.pi:
            pylab.ylim(-np.pi-0.1, np.pi+0.1)
        pylab.legend()
        pylab.savefig('dp-{fd}.eps'.format(fd=cl(fd)))  # save the figure
        pylab.show()

        # To plot the theta-omega figure.
        pylab.figure()
        pylab.xlabel("$\\omega$ / $rad*s^{-1}$", size=14)
        pylab.ylabel("$\\theta$ / rad", size=14)
        for delt in delts:
            fn = "dp-{delt}-{endt}-{fd}.log".format(
                delt=cl(delt), endt=cl(endt), fd=cl(fd)
            )
            print fn
            with open(fn) as fi:
                a = np.array([map(float, each.split())
                    for each in fi.read().split('\n')[:-1]]).transpose()
            a[1] = np.array(map(normalize, a[1]))
            if fd >= 0.5 and delt == 0.002:
                l = len(a[0])
                pylab.plot(a[2][:l/2], a[1][:l/2], '-', label='$\Delta t=%s$' % delt)
            else:
                pylab.plot(a[2], a[1], '-', label='$\Delta t=%s$' % delt)
        yl1, yl2 = pylab.ylim()
        if yl1 <= -np.pi and yl2 >= np.pi:
            pylab.ylim(-np.pi-0.1, np.pi+0.1)
        pylab.legend()
        pylab.savefig('dp-{fd}-ot.eps'.format(fd=cl(fd)))
        pylab.show()

if __name__ == '__main__':
    if raw_input() == '0':
        main()
    else:
        main2()
