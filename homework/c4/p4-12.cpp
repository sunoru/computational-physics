#include <cmath>
#include <random>
#include <fstream>

inline double
weight(double x)
{
    if (abs(x) > 10)
        return 0;
    return 1.0 / cosh(x);
}

void
metropolis(const char *logfilename, const int& n, const double& x0, const double& delta, const unsigned& random_seed)
{
    std::ofstream logfile(logfilename);
    logfile.precision(20);
    std::mt19937 gen(random_seed);
    std::uniform_real_distribution<> dis(0, 1);

    double x = x0;
    logfile << x << std::endl;
    for (int i = 0; i < n; ++i) {
        double x_new = x + delta * (2 * dis(gen) - 1);
        double ratio = weight(x_new) / weight(x);
        if (ratio > dis(gen)) {
            x = x_new;
            logfile << x << std::endl;
        }
    }
    logfile.close();
}
