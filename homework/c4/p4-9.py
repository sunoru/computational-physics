#!/usr/bin/python2.7
#coding:utf-8

def main():
    from p4_9_wrapped import rectangular, trapezoidal, simpson_s, simple_monte_carlo
    from os import urandom
    random_seed = int(urandom(4).encode('hex'), 16)
    random_seed = 409231522
    print "random_seed = %d" % random_seed
    exact = 2.467401100272339
    m = [[], [], [], []]
    print "\t\trectangular\ttrapezoidal\tSimpson's\tsimple Monte Carlo"
    maxn = 17
    for i in xrange(1, maxn):
        n = 2 ** i
        print "n = %d    \t" % n,
        m[0].append(rectangular(n))
        print "%.8f\t" % m[0][-1],
        m[1].append(trapezoidal(n))
        print "%.8f\t" % m[1][-1],
        m[2].append(simpson_s(n))
        print "%.8f\t" % m[2][-1],
        m[3].append(simple_monte_carlo(n, random_seed))
        print "%.8f\t" % m[3][-1]
    print "Difference from exact value:"
    for i in xrange(maxn-1):
        n = 2 ** i
        print "n = %d    \t" % n,
        print "%.8f\t" % (m[0][i] - exact),
        print "%.8f\t" % (m[1][i] - exact),
        print "%.8f\t" % (m[2][i] - exact),
        print "%.8f\t" % (m[3][i] - exact)

if __name__ == "__main__":
    main()

