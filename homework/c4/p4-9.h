double
rectangular(const int& n);

double
trapezoidal(const int& n);

double
simpson_s(const int& n);

double
simple_monte_carlo(const int& n, const unsigned& random_seed);
