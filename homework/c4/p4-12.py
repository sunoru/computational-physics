#!/usr/bin/python2.7
#coding:utf-8

def main():
    from p4_12_wrapped import metropolis
    import pylab
    from os import urandom
    import numpy as np
    random_seed = int(urandom(4).encode('hex'), 16)
    random_seed = 3275459943
    n = 10**6
    x0 = 0
    delta = 0.1
    filename = "p4-12.log"
    print "n = %d, x0 = %.8f, delta = %.8f, random_seed = %d" % (n, x0, delta, random_seed)
    #metropolis(filename, n, x0, delta, random_seed)
    with open(filename) as fi:
        data = np.array(map(float, fi.read().split('\n')[:-1]))
    px = np.mgrid[-8:8:0.1]
    py = 1 / np.cosh(px) / np.pi
    pylab.plot(px, py)
    pylab.hist(data, 160, (-8, 8), normed=True)
    pylab.xlabel("$x$", size=14)
    pylab.ylabel(r"$1/\mathrm{ch}x$", size=14)
    pylab.savefig(filename[:-3]+".eps")
    pylab.show()

if __name__ == "__main__":
    main()
