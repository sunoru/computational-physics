#include <cmath>
#include <random>

const double b = 1e4;
const double a = -b;

inline double
function1(double x)
{
    if (abs(x) > 25)
        return 0;
    return x * x / cosh(x);
}

inline double
function2(double x)
{
    if (abs(x) > 10)
        return 0;
    return 1 / cosh(x);
}

inline double
calc_h(const int& n)
{
    return (b - a) / n;
}

inline double
rectangular_calc(const double& h, const int& n, double (*f)(double))
{
    double result = 0;
    for (int i = 0; i < n; ++i) {
        double xi = i * h + a;
        result += f(xi);
    }
    // result *= h;
    return result;
}

double
rectangular(const int& n)
{
    double h = calc_h(n);
    double f1 = rectangular_calc(h, n, &function1);
    double f2 = rectangular_calc(h, n, &function2);
    return f1 / f2;
}

inline double
trapezoidal_calc(const double& h, const int& n, double (*f)(double))
{
    double result = 0;
    for (int i = 0; i <= n; ++i) {
        double xi = i * h + a;
        result += f(xi);
    }
    result -= (f(a) + f(b)) / 2;  // should be zero.
    // result *= h;
    return result;
}

double
trapezoidal(const int& n)
{
    double h = calc_h(n);
    double f1 = trapezoidal_calc(h, n, &function1);
    double f2 = trapezoidal_calc(h, n, &function2);
    return f1 / f2;
}

inline double
simpson_s_calc(const double& h, const int& n, double (*f)(double))
{
    double result = 0;
    for (int i = 0; i < n / 2; ++i) {
        double xi = 2 * i * h - a;
        result += f(xi) + 2 * f(xi + h);
    }
    result *= 2;
    result -= f(a) - f(b);
    // result *= h / 3;
    return result;
}

double
simpson_s(const int& n)
{
    double h = a * 2.0 / n;
    double f1 = simpson_s_calc(h, n, &function1);
    double f2 = simpson_s_calc(h, n, &function2);
    return f1 / f2;
}

double
simple_monte_carlo_calc(const int& n, const unsigned& random_seed, double (*f)(double))
{
    std::mt19937 gen(random_seed);
    std::uniform_real_distribution<> dis(a, b);
    double result = 0;
    for (int i = 0; i < n; ++i) {
        result += f(dis(gen));
    }
    // result *= (b - a) / n;
    return result;
}

double
simple_monte_carlo(const int& n, const unsigned& random_seed)
{
    double f1 = simple_monte_carlo_calc(n, random_seed, &function1);
    double f2 = simple_monte_carlo_calc(n, random_seed, &function2);
    return f1 / f2;
}

