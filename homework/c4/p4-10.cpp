#include <random>

// return the appectance ratio \Pi_N
double
monte_carlo(const int& n, const unsigned& random_seed)
{
    std::mt19937 gen(random_seed);
    std::uniform_real_distribution<> dis(-1, 1);
    int accepted = 0;
    for (int i = 0; i < n; ++i) {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y <= 1)
            accepted++;
    }
    return (double)accepted / n;
}

static int s = 0;

double inline
poor_rand()
{
    s = (1277 * s) % 131072;
    return (double)s / 131072;
}

double
poor_monte_carlo(const int& n, const unsigned& random_seed)
{
    s = random_seed;
    int accepted = 0;
    for (int i = 0; i < n; ++i) {
        double x = 2 * poor_rand() - 1;
        double y = 2 * poor_rand() - 1;
        if (x*x + y*y <= 1)
            accepted++;
    }
    return (double)accepted / n;
}


