#!/usr/bin/python2.7
#coding:utf-8

def main(monte_carlo, filename, l, r):
    from os import urandom
    import numpy as np
    x = np.mgrid[l:r]
    y = np.zeros(len(x))
    random_seed = int(urandom(4).encode('hex'), 16)
    random_seed = 131071
    print "random_seed = %d" %random_seed
    for n in x:
        if n % 100 != 0:
            y[n-l] = y[n-l-1]
            continue
        random_seed += 1
        y[n-l] = (monte_carlo(n, random_seed) - np.pi / 4)**2
        if n % 20000 == 0:
            print n, ' ', y[n-l]
    with open(filename, 'w') as fo:
        y.dump(fo)
    print "saved"

def main2(filename):
    import pylab
    import numpy as np
    with open(filename) as fi:
        y = np.load(fi)
    x = np.mgrid[10**3:10**7]
    z = np.array([y[i * 1000:i * 1000 + 999].mean() for i in xrange(len(x) / 1000)])
    px = np.mgrid[10**3:10**7:1000]+1000
    pylab.plot(px, z, 'o')
    pylab.xlabel("$N$", size=14)
    pylab.ylabel("$E^2_N$", size=14)
    pylab.xscale("log")
    pylab.yscale("log")
    pylab.show()


if __name__ == "__main__":
    from p4_10_wrapped import monte_carlo, poor_monte_carlo
    #main(monte_carlo, "p4-10b.dat", 10**3, 10**7)
    main2("p4-10b.dat")
    #import sys
    #l, r = map(int, (sys.argv[1], sys.argv[2]))
    #main(poor_monte_carlo, "p4-10c-%s.dat" % sys.argv[3], l, r)
    main2("p4-10c.dat")

