#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int
error(const char *message)
{
    cerr << message << endl;
    return 1;
}

bool
input(char *argv[], vector<double>& t, vector<double>& x, int& n)
{
    double p1, p2;
    try {
        t.clear(); x.clear();
        ifstream fi (argv[1]);
        for (int i = 0; fi >> p1 >> p2; ++i) {
            t.push_back(p1);
            x.push_back(p2);
        }
        n = t.size();
    } catch (exception e) {
        return false;
    }
    return true;
}

bool
interpolate(vector<double> const t, vector<double> const x, int const n, vector<double>& p)
{
    p.resize(n);
    for (int i = 0; i < n; ++i) {
        p[i] = 1;
        for (int j = 0; j < n; ++j) {
            if (i == j)
                continue;
            p[i] *= t[i] - t[j];
        }
        p[i] = x[i] / p[i];
    }
    return true;
}

double 
calc(double const pt, vector<double> const t, vector<double> const p, int const n)
{
    double r = 0;
    for (int i = 0; i < n; ++i) {
        double q = p[i];
        for (int j = 0; j < n; ++j) {
            if (i == j)
                continue;
            q *= pt - t[j];
        }
        r += q;
    }
    return r;
}

int
main(int argc, char *argv[])
{
    vector<double> t, x, p;
    int n;
    double pt;
    if (argc != 2 || !input(argv, t, x, n)) {
        return error("Input error");
    }
    if (!interpolate(t, x, n, p)) {
        return error("Unkown error");
    }
    cout.setf(ios::scientific);
    cout.precision(8);
    cout << "t0: " << t[0] << "\ttn: " << t[n-1] << endl;
    cout << "Use EOF to exit." << endl;
    for (; ;) {
        cout << "Input t: ";
        if (!(cin >> pt))
            break;
        if (pt < t[0] || pt > t[n-1]) {
            cout << "Out of range!" << endl;
            continue;
        }
        cout << "x: " << calc(pt, t, p, n) << endl;
    }

    return 0;
}
