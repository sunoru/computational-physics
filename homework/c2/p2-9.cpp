#include <fstream>
#include <sstream>
#include <cmath>


inline double
force(double x)
{
    return 48 / pow(x, 13) - 24 / pow(x, 7);
}

inline double
energy(double x)
{
    return 4 / pow(x, 6) - 4 / pow(x, 12);
}

void
euler(std::ofstream& logfile, const double& dt, const double& tt)
{
    // initial condition.
    double x = 2, v = 0, t = 0;
    double dv, dx;
    logfile << "#t\tx\tE" << std::endl;
    logfile.setf(std::ios::scientific);
    logfile.precision(20);
    do {
        logfile << t << "\t" << x << "\t" << energy(x) << std::endl;
        dv = dt * force(x);
        dx = v * dt;
        v += dv;
        t += dt;
        x += dx;
    } while (t < tt);
}

int
main(int argc, char *argv[])
{
    if (argc != 4)
        return 1;
    double dt, tt;
    std::stringstream s1(argv[1]);
    std::stringstream s2(argv[2]);
    s1 >> dt;
    s2 >> tt;
    std::ofstream logfile (argv[3]);
    euler(logfile, dt, tt);
    logfile.close();

    return 0;
}
