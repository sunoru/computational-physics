#!/usr/bin/python2.7

def main():
    import os
    import pylab
    import numpy as np
    dts = [(0.1 ** i, i) for i in range(2, 6)]
    data = {}
    tt = 10.0
    for dt, i in dts:
        filename = "2-12_%s.log" % i
        s = "./2-12.out %s %s %s" % (dt, tt, filename)
        print s
        os.system(s)
        with open(filename) as fi:
            raw = map(lambda x: map(float, x.split()),
                filter(lambda x: not x.startswith('#') and len(x) > 0,
                    fi.read().split('\n')
                )
            )
        data[i] = np.array(raw).transpose()

    print "ploting x to t..."
    for dt, i in dts:
        pylab.plot(data[i][0], data[i][1], label="t = 0.%s1" % ('0'*(i-1)))
    pylab.title("x-t", size=14)
    pylab.xlabel("t", size=14)
    pylab.ylabel("x", size=14)
    pylab.legend()
    pylab.savefig("2-12_x_t.eps")
    pylab.show()

    print "ploting E to t..."
    pylab.figure()
    for dt, i in dts:
        pylab.plot(data[i][0], data[i][2], label="t = 0.%s1" % ('0'*(i-1)))
    pylab.title("E-t", size=14)
    pylab.xlabel("t", size=14)
    pylab.ylabel("E", size=14)
    pylab.legend()
    pylab.savefig("2-12_E_t.eps")
    pylab.show()

    print "ploting E(T) to h..."
    pylab.figure()
    ex = [dt for (dt, i) in dts]
    ey = [data[i][2][-1] for (dt, i) in dts]
    pylab.plot(ex, ey, 'o-')
    pylab.title("E(T)-h", size=14)
    pylab.xlabel("h", size=14)
    pylab.ylabel("E(T)", size=14)
    pylab.xscale("log")
    pylab.yscale("log")
    pylab.savefig("2-12_E_h.eps")
    pylab.show()

    print "All Done"
    

if __name__ == "__main__":
    main()

