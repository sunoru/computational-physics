#include <fstream>
#include <sstream>
#include <cmath>


inline double
force(double x)
{
    return 48 / pow(x, 13) - 24 / pow(x, 7);
}

inline double
energy(double x)
{
    return 4 / pow(x, 6) - 4 / pow(x, 12);
}

void
verlet(std::ofstream& logfile, const double& dt, const double& tt)
{
    // initial condition.
    double x0 = 2, v0 = 0, t = 0, x1 = x0 + v0 * dt, x = x0;
    logfile << "#t\tx\tE" << std::endl;
    logfile.setf(std::ios::scientific);
    logfile.precision(20);
    logfile << t << "\t" << x << "\t" << energy(x) << std::endl;
    x = x1;
    t += dt;
    do {
        x = 2 * x1 - x0 + force(x1) * dt * dt;
        logfile << t << "\t" << x << "\t" << energy(x) << std::endl;
        x0 = x1;
        x1 = x;
        t += dt;
    } while (t < tt);
}

int
main(int argc, char *argv[])
{
    if (argc != 4)
        return 1;
    double dt, tt;
    std::stringstream s1(argv[1]);
    std::stringstream s2(argv[2]);
    s1 >> dt;
    s2 >> tt;
    std::ofstream logfile (argv[3]);
    verlet(logfile, dt, tt);
    logfile.close();

    return 0;
}
