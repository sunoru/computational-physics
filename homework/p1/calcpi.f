      real function calcpi(n, seed)
      implicit none
      integer n, seed
      integer i, accepted
      real x, y, ds
      call srand(seed)
      accepted = 0
      do 10 i = 1, n
          x = rand() - 0.5
          y = rand() - 0.5
          ds = x*x + y*y
          if (ds .LT. 0.25) then
              accepted = accepted + 1
          end if
10     continue
      calcpi = real(accepted) / n * 4.0
      return
      end
